﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TextEventHandler : MonoBehaviour,IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{
    public int FontSizeMouseEnter;
    public int FontSizeMouseExit;
    public int SceneNumber;

    public IEnumerator IncreaseToTargetFontSize(float t, Text i,int taretSize)
    {
  
        while (i.fontSize < taretSize)
        {
            i.fontSize++;
            yield return null;
        }
    }



    public IEnumerator DecreaseToTargetFontSize(float t, Text i, int taretSize)
    {

        while (i.fontSize > taretSize)
        {
            i.fontSize--;
            yield return null;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("Mouse enter");
        Text text = GetComponent<Text>();
        StartCoroutine(IncreaseToTargetFontSize(1, text, FontSizeMouseEnter));
  

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Debug.Log("Mouse remove");
        Text text = GetComponent<Text>();
        StartCoroutine(DecreaseToTargetFontSize(1, text, FontSizeMouseExit));


    }

    //Do this when the mouse is clicked over the selectable object this script is attached to.
    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log(this.gameObject.name + " Was Clicked.");

    }

    // Use this for initialization
    void Start () {
        StartCoroutine(FadeTextToFullAlpha(1f, GetComponent<Text>()));
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public IEnumerator FadeTextToFullAlpha(float t, Text i)
    {
        i.color = new Color(i.color.r, i.color.g, i.color.b, 0);
        while (i.color.a < 1.0f)
        {
            i.color = new Color(i.color.r, i.color.g, i.color.b, i.color.a + (Time.deltaTime / t));
            yield return null;
        }
    }

}
